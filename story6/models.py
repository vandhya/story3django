from django.db import models

class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=30)

    def __str__(self):
        return self.namaKegiatan

class Peserta(models.Model):
    namaPeserta = models.CharField(max_length=30)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, related_name='peserta')

    def __str__(self):
        return self.namaPeserta