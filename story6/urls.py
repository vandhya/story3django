from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.story6, name='story6'),
]
