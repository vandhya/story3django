from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from . import forms

# Create your views here.
def story6(request):
    pesertas = Peserta.objects.all()
    return render(request, 'story6/isi.html', {'pesertas':pesertas})

def createKegiatan(request):
    if request.method == "POST":
        form = forms.CreateKegiatan(request.POST)
        if form.is_valid():
            return redirect('story6:story6')
    else:
        form = forms.CreateKegiatan()
    return render(request, 'story6/tambahKegiatan.html', {'form':form})

def tambahPeserta(request):
    if request.method == "POST":
        form = forms.CreateKegiatan(request.POST)
        if form.is_valid():
            return redirect('story6:story6')
    else:
        form = forms.CreateKegiatan()
    return render(request, 'story6/tambahPeserta.html', {'form':form})
    