from django import forms
from . import models

class CreateKegiatan(forms.ModelForm):
    class Meta:
        model = models.Kegiatan
        fields = ['namaKegiatan']

class TambahPeserta(forms.ModelForm):
    class Meta:
        model = models.Peserta
        fields = ['namaPeserta', 'kegiatan']