from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.story5, name='story5'),
    path('creatematkul', views.creatematkul, name='creatematkul'),
    path('<slug:slug>/', views.matkul_detail, name='matkul_detail'),
    path('delete/<int:id>', views.delete, name='delete'),
]
