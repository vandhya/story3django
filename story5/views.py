from django.shortcuts import render, redirect
from .models import MataKuliah
from . import forms

# Create your views here.
def story5(request):
    matkuls = MataKuliah.objects.all()
    return render(request, 'story5/index.html', {'matkuls':matkuls})

def creatematkul(request):
    if request.method == "POST":
        form = forms.CreateMatkul(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story5:story5')
    else:
        form = forms.CreateMatkul()
    return render(request, 'story5/create_matkul.html', {'form':form})

def matkul_detail(request, slug):
    matkul = MataKuliah.objects.get(slug=slug)
    return render(request, 'story5/matkul_detail.html', { 'matkul':matkul})

def delete(request,id):
    matkul = MataKuliah.objects.get(id=id)
    matkul.delete()
    return redirect('story5:story5')