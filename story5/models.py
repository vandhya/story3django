from django.db import models
from django.template.defaultfilters import slugify

# Create your models here.
class MataKuliah(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField()
    dosen = models.TextField()
    sks = models.PositiveSmallIntegerField()
    semester = models.CharField(max_length=50)
    ruang = models.CharField(max_length=50)
    desc = models.TextField()
    
    def save(self, *args, **kwargs):
        if not self.id:
            # Newly created object, so set slug
            self.slug = slugify(self.name)

        super(MataKuliah, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name

#    class Meta:
#        verbose_name = _("MataKuliah")
#        verbose_name_plural = _("MataKuliahs")



#    def get_absolute_url(self):
#        return reverse("MataKuliah_detail", kwargs={"pk": self.pk})
